package com.example.jira.soap.endpoints;


import com.example.jira.persistence.entities.Ticket;
import com.example.jira.service.TicketService;
import com.example.jira.ws.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.List;
import java.util.Optional;

@Endpoint
public class TicketEndpoint {
    private static final String NAMESPACE_URI = "http://jira.example.com/ws";

    private TicketService ticketService;

    @Autowired
    public TicketEndpoint(TicketService ticketService) {
        this.ticketService = ticketService;
    }


    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetTicketByIdRequest")
    @ResponsePayload
    public GetTicketByIdResponse getPersonById(@RequestPayload GetTicketByIdRequest request) {
        GetTicketByIdResponse getTicketByIdResponse = new GetTicketByIdResponse();
        Optional<Ticket> ticket = ticketService.getTicketById(request.getId());
        if(ticket.isPresent()) {
        TicketById ob = mapTicketToTicketById(ticket.get());
        getTicketByIdResponse.setTicketById(ob);}
        return getTicketByIdResponse;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetTicketByNameRequest")
    @ResponsePayload
    public GetTicketByNameResponse getPersonByName(@RequestPayload GetTicketByNameRequest request) {
        GetTicketByNameResponse getTicketByIdResponse = new GetTicketByNameResponse();
        List<Ticket> tickets = ticketService.getTicketByName(request.getName());
        tickets.stream().map(this::mapTicketToTicketById).forEach(getTicketByIdResponse.getTicketsByName()::add);
        return getTicketByIdResponse;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "AddTicketRequest")
    @ResponsePayload
    public AddTicketResponse addTicket(@RequestPayload AddTicketRequest request) {
        AddTicketResponse response = new AddTicketResponse();
        ServiceStatus serviceStatus = new ServiceStatus();
        Optional<Ticket> ticket = mapAddTicketRequestToTicket(request);
        if(ticket.isPresent()) {
            long flag = ticketService.addTicket(ticket.get());
            if (flag == 0) {
                serviceStatus.setStatusCode("CONFLICT");
                serviceStatus.setMessage("Content Already Available");
            } else {
                TicketById ticketById =  mapTicketToTicketById(flag, ticket.get());
                response.setTicketById(ticketById);
                serviceStatus.setStatusCode("SUCCESS");
                serviceStatus.setMessage("Content Added Successfully");
            }
            response.setServiceStatus(serviceStatus);
        }
        return response;
    }



    private TicketById mapTicketToTicketById(Ticket ticket) {
        TicketById ticketById = new TicketById();
        ticketById.setId(ticket.getId());
        ticketById.setName(ticket.getName());
        ticketById.setEmail(ticket.getEmail());
        ticketById.setIdPersonCreator(ticket.getIdPersonCreator());
        ticketById.setIdPersonAssigned(ticket.getIdPersonAssigned());
        ticketById.setCreationDatetime(ticket.getCreationDateTime());
        return ticketById;
    }

    private TicketById mapTicketToTicketById(long flag, Ticket ticket) {
        TicketById ticketById = new TicketById();
        ticketById.setId(flag);
        ticketById.setName(ticket.getName());
        ticketById.setEmail(ticket.getEmail());
        ticketById.setIdPersonCreator(ticket.getIdPersonCreator());
        ticketById.setIdPersonAssigned(ticket.getIdPersonAssigned());
        ticketById.setCreationDatetime(ticket.getCreationDateTime());
         return ticketById;
    }
    private Optional<Ticket> mapAddTicketRequestToTicket(AddTicketRequest request) {
        if(request!=null) {
            Ticket ticket = new Ticket();
            ticket.setName(request.getName());
            ticket.setEmail(request.getEmail());
            ticket.setIdPersonCreator(request.getIdPersonCreator());
            ticket.setIdPersonAssigned(request.getIdPersonAssigned());
            ticket.setCreationDateTime(request.getCreationDatetime());
            ticket.setTicketCloseTime(request.getTicketCloseDatetime());
            return Optional.of(ticket);
        }

       return Optional.empty();
    }
}
