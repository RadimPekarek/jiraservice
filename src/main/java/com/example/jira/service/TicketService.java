package com.example.jira.service;

import com.example.jira.persistence.entities.Ticket;

import java.util.List;
import java.util.Optional;

public interface TicketService {
    Optional<Ticket> getTicketById(Long id);

    List<Ticket> getTicketByName(String email);

    long addTicket(Ticket ticket);
}
