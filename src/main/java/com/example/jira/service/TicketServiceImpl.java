package com.example.jira.service;

import com.example.jira.persistence.entities.Ticket;
import com.example.jira.persistence.repository.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class TicketServiceImpl implements TicketService {
    private TicketRepository ticketRepository;

    @Autowired
    public TicketServiceImpl(TicketRepository ticketRepository) {
        this.ticketRepository = ticketRepository;
    }

    @Override
    public Optional<Ticket> getTicketById(Long id) {
        return Optional.ofNullable(ticketRepository.getTicketById(id));
    }

    @Override
    public List<Ticket> getTicketByName(String email) {
        return ticketRepository.getTicketByName(email);
    }

    @Override
    @Transactional(rollbackFor = {Exception.class, RuntimeException.class})
    public long addTicket(Ticket ticket) {
        return ticketRepository.addTicket(ticket);
    }
}
