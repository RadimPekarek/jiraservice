package com.example.jira;

import com.example.jira.persistence.repository.TicketRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@SpringBootApplication
public class JiraApplication {

    public static void main(String[] args) {

        SpringApplication.run(JiraApplication.class, args);
    /*    AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(JiraApplication.class);
        TicketRepository ticketRepository = context.getBean(TicketRepository.class);
        System.out.println(ticketRepository.getTicketById(1L));*/
    }

}
