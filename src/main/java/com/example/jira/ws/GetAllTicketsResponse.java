//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.03.05 at 09:05:40 dop. CET 
//


package com.example.jira.ws;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AllTickets" type="{http://jira.example.com/ws}TicketById" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "allTickets"
})
@XmlRootElement(name = "GetAllTicketsResponse")
public class GetAllTicketsResponse {

    @XmlElement(name = "AllTickets", required = true)
    protected List<TicketById> allTickets;

    /**
     * Gets the value of the allTickets property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the allTickets property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAllTickets().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TicketById }
     */
    public List<TicketById> getAllTickets() {
        if (allTickets == null) {
            allTickets = new ArrayList<TicketById>();
        }
        return this.allTickets;
    }

}
