package com.example.jira.persistence.mappers;

import com.example.jira.persistence.entities.Ticket;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class TicketMapper implements RowMapper<Ticket> {
    @Override
    public Ticket mapRow(ResultSet resultSet, int i) throws SQLException {
        Ticket ticket = new Ticket();
        ticket.setId(resultSet.getLong("id"));
        ticket.setName(resultSet.getString("name"));
        ticket.setEmail(resultSet.getString("email"));
        ticket.setIdPersonCreator(resultSet.getLong("id_person_creator"));
        ticket.setIdPersonAssigned(resultSet.getLong("id_person_assigned"));
        if(resultSet.getTimestamp("creation_datetime")!=null)
        ticket.setCreationDateTime(resultSet.getTimestamp("creation_datetime").toLocalDateTime());
        if(resultSet.getTimestamp("ticket_close_datetime")!=null)
        ticket.setTicketCloseTime(resultSet.getTimestamp("ticket_close_datetime").toLocalDateTime());
        return ticket;
    }
}
