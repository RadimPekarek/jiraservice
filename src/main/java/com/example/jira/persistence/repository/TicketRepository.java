package com.example.jira.persistence.repository;

import com.example.jira.persistence.entities.Ticket;
import com.example.jira.persistence.mappers.TicketMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.List;

@Repository
public class TicketRepository {

    @Value("${select.by.id}")
    private String selectById;
    @Value("${select.by.name}")
    private String selectByName;
    @Value("${insert.ticket}")
    private String insertNewTicket;

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public TicketRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;

    }

    public Ticket getTicketById(Long id) {
        try {
            return jdbcTemplate.queryForObject(selectById,
                    new Object[]{id}, new TicketMapper());
        } catch (
                EmptyResultDataAccessException e) {
            return null;
        }
    }

    public List<Ticket> getTicketByName(String name) {
        try {
            return jdbcTemplate.query(selectByName,
                    new Object[]{name}, new TicketMapper());
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    public long addTicket(Ticket ticket) {
        GeneratedKeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update(con -> {
            PreparedStatement statement = con.prepareStatement(insertNewTicket, Statement.RETURN_GENERATED_KEYS);

            statement.setString(1, ticket.getName());
            statement.setString(2, ticket.getEmail());
            statement.setLong(3, ticket.getIdPersonCreator());
            statement.setLong(4, ticket.getIdPersonAssigned());
            if (ticket.getCreationDateTime() != null) {
                statement.setTimestamp(5, Timestamp.valueOf(ticket.getCreationDateTime()));
            } else {
                statement.setTimestamp(5, null);
            }
            if (ticket.getTicketCloseTime() != null) {
                statement.setTimestamp(6, Timestamp.valueOf(ticket.getTicketCloseTime()));
            } else {
                statement.setTimestamp(6, null);
            }
            return statement;
        }, holder);
        long primaryKey = 0;
        if (holder.getKeys() != null && holder.getKeys().size() > 1) {
            primaryKey = (long) holder.getKeys().get("id");
        } else {
            if (holder.getKey() != null)
                primaryKey = holder.getKey().longValue();
        }

        return primaryKey;
    }

}
