package com.example.jira.persistence.entities;

import java.time.LocalDateTime;
import java.util.Objects;

public class Ticket {
    private Long id;
    private String name;
    private String email;
    private Long idPersonCreator;
    private Long idPersonAssigned;
    private LocalDateTime creationDateTime;
    private LocalDateTime ticketCloseTime;

    public Ticket() {
    }

    public Ticket(String name, String email, Long idPersonCreator, Long idPersonAssigned, LocalDateTime creationDateTime, LocalDateTime ticketCloseTime) {
        this.name = name;
        this.email = email;
        this.idPersonCreator = idPersonCreator;
        this.idPersonAssigned = idPersonAssigned;
        this.creationDateTime = creationDateTime;
        this.ticketCloseTime = ticketCloseTime;
    }

    public Ticket(Long id, String name, String email, Long idPersonCreator, Long idPersonAssigned, LocalDateTime creationDateTime, LocalDateTime ticketCloseTime) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.idPersonCreator = idPersonCreator;
        this.idPersonAssigned = idPersonAssigned;
        this.creationDateTime = creationDateTime;
        this.ticketCloseTime = ticketCloseTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getIdPersonCreator() {
        return idPersonCreator;
    }

    public void setIdPersonCreator(Long idPersonCreator) {
        this.idPersonCreator = idPersonCreator;
    }

    public Long getIdPersonAssigned() {
        return idPersonAssigned;
    }

    public void setIdPersonAssigned(Long idPersonAssigned) {
        this.idPersonAssigned = idPersonAssigned;
    }

    public LocalDateTime getCreationDateTime() {
        return creationDateTime;
    }

    public void setCreationDateTime(LocalDateTime creationDateTime) {
        this.creationDateTime = creationDateTime;
    }

    public LocalDateTime getTicketCloseTime() {
        return ticketCloseTime;
    }

    public void setTicketCloseTime(LocalDateTime ticketCloseTime) {
        this.ticketCloseTime = ticketCloseTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Ticket)) return false;
        Ticket ticket = (Ticket) o;
        return Objects.equals(getId(), ticket.getId()) &&
                Objects.equals(getName(), ticket.getName()) &&
                Objects.equals(getEmail(), ticket.getEmail()) &&
                Objects.equals(getIdPersonCreator(), ticket.getIdPersonCreator()) &&
                Objects.equals(getIdPersonAssigned(), ticket.getIdPersonAssigned()) &&
                Objects.equals(getCreationDateTime(), ticket.getCreationDateTime()) &&
                Objects.equals(getTicketCloseTime(), ticket.getTicketCloseTime());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getEmail(), getIdPersonCreator(), getIdPersonAssigned(), getCreationDateTime(), getTicketCloseTime());
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", idPersonCreator=" + idPersonCreator +
                ", idPersonAssigned=" + idPersonAssigned +
                ", creationDateTime=" + creationDateTime +
                ", ticketCloseTime=" + ticketCloseTime +
                '}';
    }
}
