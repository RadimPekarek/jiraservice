package com.example.jira;

import com.example.jira.persistence.entities.Ticket;
import com.example.jira.persistence.repository.TicketRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = JiraApplication.class)
public class TicketRepositoryTest {

    private static Logger log = LoggerFactory.getLogger(TicketRepositoryTest.class);
    @Autowired
    TicketRepository ticketRepository;

    @Test
    public void getTicketById() {
        Ticket ticket =ticketRepository.getTicketById(1L);
        log.info("personInfo {}", ticket);
        assertNotNull("Should not be null", ticket);

    }

    @Test
    public void getTicketByName() {
        List<Ticket> ticket = ticketRepository.getTicketByName("rolling incident");
        log.info("personInfo {}", ticket);
        assertFalse(ticket.isEmpty());

    }

    @Test
    public void addTicket() {
        Ticket ticket =new Ticket();
        ticket.setName("rolling incident resolved");
        ticket.setEmail("radek.kruta@seznam.cz");
        ticket.setIdPersonCreator(1L);
        ticket.setIdPersonAssigned(2L);
        ticket.setCreationDateTime(LocalDateTime.now(Clock.systemUTC()));
        ticket.setCreationDateTime(LocalDateTime.now(Clock.systemUTC()).plusHours(2));
        long flag = ticketRepository.addTicket(ticket);
        log.info("ticket {}", ticket);
        assertEquals(2, flag);
    }
}
