CREATE TABLE ticket (
                        id bigint NOT NUll AUTO_INCREMENT,
                        name varchar(500) NOT NULL,
                        email varchar(100),
                        id_person_creator bigint,
                        id_person_assigned bigint,
                        creation_datetime timestamp,
                        ticket_close_datetime timestamp,
                        PRIMARY KEY (id)
);